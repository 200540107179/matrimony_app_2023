import 'package:flutter/material.dart';
import 'package:matrimony_app/api_crud.dart';
import 'package:matrimony_app/api_demo.dart';
import 'package:matrimony_app/database/database.dart';
import 'package:matrimony_app/home_page/add_user_page.dart';
import 'package:matrimony_app/home_page/firstpage.dart';
import 'package:matrimony_app/home_page/home_page.dart';
import 'package:matrimony_app/home_page/search_page.dart';
import 'package:matrimony_app/home_page/user_list_page.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    MyDatabase().copyPasteAssetFileToRoot().then((value) {});
    return MaterialApp(
      title: 'Flutter Demo',
      //color: Colors.yellow,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Api_CRUD(),
    );
  }
}
