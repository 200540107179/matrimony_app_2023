import 'dart:convert';
/// Name : "Corey Parker"
/// DOB : "1979-09-21T23:43:01.255Z"
/// Gender : "Man"
/// Height : "Height 10"
/// Weigth : "Weigth 10"
/// Country : "Cambridgeshire"
/// State : "North Carolina"
/// City : "Port Arthur"
/// Religion : "breeze"
/// Education : "Officer"
/// Profeasion : "Identity"
/// id : "10"

UserListItem userListItemFromJson(String str) => UserListItem.fromJson(json.decode(str));
String userListItemToJson(UserListItem data) => json.encode(data.toJson());
class UserListItem {
  UserListItem({
      String? name, 
      String? dob, 
      String? gender, 
      String? height, 
      String? weigth, 
      String? country, 
      String? state, 
      String? city, 
      String? religion, 
      String? education, 
      String? profeasion, 
      String? id,}){
    _name = name;
    _dob = dob;
    _gender = gender;
    _height = height;
    _weigth = weigth;
    _country = country;
    _state = state;
    _city = city;
    _religion = religion;
    _education = education;
    _profeasion = profeasion;
    _id = id;
}

  UserListItem.fromJson(dynamic json) {
    _name = json['Name'];
    _dob = json['DOB'];
    _gender = json['Gender'];
    _height = json['Height'];
    _weigth = json['Weigth'];
    _country = json['Country'];
    _state = json['State'];
    _city = json['City'];
    _religion = json['Religion'];
    _education = json['Education'];
    _profeasion = json['Profeasion'];
    _id = json['id'];
  }
  String? _name;
  String? _dob;
  String? _gender;
  String? _height;
  String? _weigth;
  String? _country;
  String? _state;
  String? _city;
  String? _religion;
  String? _education;
  String? _profeasion;
  String? _id;
UserListItem copyWith({  String? name,
  String? dob,
  String? gender,
  String? height,
  String? weigth,
  String? country,
  String? state,
  String? city,
  String? religion,
  String? education,
  String? profeasion,
  String? id,
}) => UserListItem(  name: name ?? _name,
  dob: dob ?? _dob,
  gender: gender ?? _gender,
  height: height ?? _height,
  weigth: weigth ?? _weigth,
  country: country ?? _country,
  state: state ?? _state,
  city: city ?? _city,
  religion: religion ?? _religion,
  education: education ?? _education,
  profeasion: profeasion ?? _profeasion,
  id: id ?? _id,
);
  String? get name => _name;
  String? get dob => _dob;
  String? get gender => _gender;
  String? get height => _height;
  String? get weigth => _weigth;
  String? get country => _country;
  String? get state => _state;
  String? get city => _city;
  String? get religion => _religion;
  String? get education => _education;
  String? get profeasion => _profeasion;
  String? get id => _id;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Name'] = _name;
    map['DOB'] = _dob;
    map['Gender'] = _gender;
    map['Height'] = _height;
    map['Weigth'] = _weigth;
    map['Country'] = _country;
    map['State'] = _state;
    map['City'] = _city;
    map['Religion'] = _religion;
    map['Education'] = _education;
    map['Profeasion'] = _profeasion;
    map['id'] = _id;
    return map;
  }

}