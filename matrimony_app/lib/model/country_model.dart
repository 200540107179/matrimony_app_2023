class CountryModel {
  late int _Country_ID;
  late String _Country_NAME;

  int get Country_ID => _Country_ID;

  set Country_ID(int Country_ID) {
    _Country_ID = Country_ID;
  }

  String get Country_NAME => _Country_NAME;

  set Country_NAME(String Country_NAME) {
    _Country_NAME = Country_NAME;
  }
}
