class StateModel {
  late int _State_ID;
  late String _State_NAME;

  int get State_ID => _State_ID;

  set State_ID(int State_ID) {
    _State_ID = State_ID;
  }

  String get State_NAME => _State_NAME;

  set State_NAME(String State_NAME) {
    _State_NAME = State_NAME;
  }
}
