class EducationModel {
  late int _Education_ID;
  late String _Education_NAME;

  int get Education_ID => _Education_ID;

  set Education_ID(int Education_ID) {
    _Education_ID = Education_ID;
  }

  String get Education_NAME => _Education_NAME;

  set Education_NAME(String Education_NAME) {
    _Education_NAME = Education_NAME;
  }
}
