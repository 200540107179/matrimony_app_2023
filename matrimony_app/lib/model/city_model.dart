class CityModel {
  late int _City_ID;
  late String _City_NAME;

  int get City_ID => _City_ID;

  set City_ID(int City_ID) {
    _City_ID = City_ID;
  }

  String get City_NAME => _City_NAME;

  set City_NAME(String City_NAME) {
    _City_NAME = City_NAME;
  }
}
