class UserModel {
  late int _User_ID;
  late String _User_NAME;
  late String _User_DOB;
  late String _User_GENDER;
  late String _User_HEIGHT;
  late String _User_WEIGHT;
  late int _Country_ID;
  late int _State_ID;
  late int _City_ID;
   late String _Religion_ID;
   late bool _isFavouriteUser;

  bool get isFavouriteUser => _isFavouriteUser;

  set isFavouriteUser(bool isFavouriteUser) {
    _isFavouriteUser = isFavouriteUser;
  }

  String get Religion_ID => _Religion_ID;

  set Religion_ID(String Religion_ID) {
    _Religion_ID = Religion_ID;
  }
  late String _Education_ID;

  String get Education_ID => _Education_ID;

  set Education_ID(String Education_ID) {
    _Education_ID = Education_ID;
  }
  late String _Profession_ID;

  String get Profession_ID => _Profession_ID;

  set Profession_ID(String Profession_ID) {
    _Profession_ID = Profession_ID;
  }

  int get User_ID => _User_ID;

  set User_ID(int User_ID) {
    _User_ID = User_ID;
  }

  String get User_NAME => _User_NAME;

  set User_NAME(String User_NAME) {
    _User_NAME = User_NAME;
  }

  String get User_DOB => _User_DOB;

  set User_DOB(String User_DOB) {
    _User_DOB = User_DOB;
  }

  String get User_GENDER => _User_GENDER;

  set User_GENDER(String User_GENDER) {
    _User_GENDER = User_GENDER;
  }

  String get User_HEIGHT => _User_HEIGHT;

  set User_HEIGHT(String User_HEIGHT) {
    _User_HEIGHT = User_HEIGHT;
  }

  String get User_WEIGHT => _User_WEIGHT;

  set User_WEIGHT(String User_WEIGHT) {
    _User_WEIGHT = User_WEIGHT;
  }

  int get Country_ID => _Country_ID;

  set Country_ID(int Country_ID) {
    _Country_ID = Country_ID;
  }

  int get State_ID => _State_ID;

  set State_ID(int State_ID) {
    _State_ID = State_ID;
  }

  int get City_ID => _City_ID;

  set City_ID(int City_ID) {
    _City_ID = City_ID;
  }


}
