class ReligionModel {
  late int _Religion_ID;
  late String _Religion_Name;

  int get Religion_ID => _Religion_ID;

  set Religion_ID(int Religion_ID) {
    _Religion_ID = Religion_ID;
  }

  String get Religion_Name => _Religion_Name;

  set Religion_Name(String Religion_Name) {
    _Religion_Name = Religion_Name;
  }
}
