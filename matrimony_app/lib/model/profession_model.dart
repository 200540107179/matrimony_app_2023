class ProfessionModel {
  late int _Profession_ID;
  late String _Profession_NAME;

  int get Profession_ID => _Profession_ID;

  set Profession_ID(int Profession_ID) {
    _Profession_ID = Profession_ID;
  }

  String get Profession_NAME => _Profession_NAME;

  set Profession_NAME(String Profession_NAME) {
    _Profession_NAME = Profession_NAME;
  }
}
