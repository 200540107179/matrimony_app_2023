import 'package:adaptive_scrollbar/adaptive_scrollbar.dart';
import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:matrimony_app/api/rest_client.dart';
import 'package:matrimony_app/database/database.dart';
import 'package:matrimony_app/model/city_model.dart';
import 'package:matrimony_app/model/country_model.dart';
import 'package:matrimony_app/model/state_model.dart';
import 'package:matrimony_app/model/user_model.dart';
import 'package:dio/dio.dart';
import 'package:http/http.dart' as http;

class AddUser extends StatefulWidget {
  late UserModel? model;

  AddUser({required this.model});

  @override
  State<AddUser> createState() => _AddUserState();
}

class _AddUserState extends State<AddUser> {
  late CityModel model;
  late CountryModel model1;
  late StateModel model2;

  bool isGetCountry = true;
  bool isGetCity = true;
  bool isGetState = true;

  final _formKey = GlobalKey<FormState>();

  // late TextEditingController cityController;

  //late TextEditingController

  late TextEditingController nameController;
  late TextEditingController dateController;
  late TextEditingController genderController;
  late TextEditingController heightController;
  late TextEditingController weightController;
  late TextEditingController educationController;
  late TextEditingController professionController;
  late TextEditingController religionController;

  @override
  void initState() {
    super.initState();

    nameController = TextEditingController(
        text: widget.model != null ? widget.model!.User_NAME.toString() : '');

    genderController = TextEditingController(
        text: widget.model != null ? widget.model!.User_GENDER.toString() : '');

    dateController = TextEditingController(
        text: widget.model != null ? widget.model!.User_DOB.toString() : '');

    heightController = TextEditingController(
        text: widget.model != null ? widget.model!.User_HEIGHT.toString() : '');

    weightController = TextEditingController(
        text: widget.model != null ? widget.model!.User_WEIGHT.toString() : '');

    educationController = TextEditingController(
        text:
            widget.model != null ? widget.model!.Education_ID.toString() : '');

    professionController = TextEditingController(
        text:
            widget.model != null ? widget.model!.Profession_ID.toString() : '');

    religionController = TextEditingController(
        text: widget.model != null ? widget.model!.Religion_ID.toString() : '');
  }

  int getDropDownSelectedPosition(dynamic id, List<CountryModel> countries) {
    if (id != null) {
      for (int i = 0; i < countries.length; i++) {
        if (id == countries[i].Country_ID) {
          return i;
        }
      }
    }
    return 0;
  }

  int getDropDownSelectedPosition1(dynamic id, List<StateModel> state) {
    if (id != null) {
      for (int i = 0; i < state.length; i++) {
        if (id == state[i].State_ID) {
          return i;
        }
      }
    }
    return 0;
  }

  int getDropDownSelectedPosition2(dynamic id, List<CityModel> city) {
    if (id != null) {
      for (int i = 0; i < city.length; i++) {
        if (id == city[i].City_ID) {
          return i;
        }
      }
    }
    return 0;
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text('Add User'),
        ),
        body: SingleChildScrollView(
          reverse: true,
          physics: BouncingScrollPhysics(),
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                Container(
                  margin: EdgeInsets.fromLTRB(0, 5, 0, 7),
                  width: double.infinity,
                  child: FutureBuilder<List<CountryModel>>(
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data != null) {
                        if (isGetCountry) {
                          model1 = widget.model != null
                              ? snapshot.data![getDropDownSelectedPosition(
                                  widget.model!.Country_ID, snapshot.data!)]
                              : snapshot.data![0];
                        }
                        return DropdownButtonHideUnderline(
                          child: DropdownButton2(
                            isExpanded: true,
                            items: snapshot.data!
                                .map(
                                  (item) => DropdownMenuItem<CountryModel>(
                                    value: item,
                                    child: Text(
                                      item.Country_NAME.toString(),
                                      style: const TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ),
                                )
                                .toList(),
                            value: model1,
                            onChanged: (value) {
                              setState(
                                () {
                                  isGetCountry = false;
                                  model1 = value!;
                                },
                              );
                            },
                            icon: const Icon(
                              Icons.arrow_drop_down_outlined,
                            ),
                            buttonDecoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14),
                              border: Border.all(
                                color: Colors.black26,
                              ),
                              color: Colors.blue.shade100,
                            ),
                            dropdownDecoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14),
                              color: Colors.blue.shade100,
                            ),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                    future: isGetCountry
                        ? MyDatabase().getCountryListFromTbl()
                        : null,
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 5, 0, 7),
                  width: double.infinity,
                  child: FutureBuilder<List<StateModel>>(
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data != null) {
                        if (isGetState) {
                          print("DATA:::${snapshot.data!.length}");
                          // model1 = snapshot.data![getDropDownSelectedPosition(
                          //     widget.model!.Country_ID, snapshot.data!)];
                          model2 = widget.model != null
                              ? snapshot.data![getDropDownSelectedPosition1(
                                  widget.model!.State_ID, snapshot.data!)]
                              : snapshot.data![0];
                        }
                        return DropdownButtonHideUnderline(
                          child: DropdownButton2(
                            isExpanded: true,
                            items: snapshot.data!
                                .map((item) => DropdownMenuItem<StateModel>(
                                      value: item,
                                      child: Text(
                                        item.State_NAME.toString(),
                                        style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ))
                                .toList(),
                            value: model2,
                            onChanged: (value) {
                              setState(
                                () {
                                  isGetState = false;
                                  model2 = value!;
                                },
                              );
                            },
                            icon: const Icon(
                              Icons.arrow_drop_down_outlined,
                            ),
                            buttonDecoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14),
                              border: Border.all(
                                color: Colors.black26,
                              ),
                              color: Colors.blue.shade100,
                            ),
                            dropdownDecoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14),
                              color: Colors.blue.shade100,
                            ),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                    future:
                        isGetState ? MyDatabase().getStateListFromTbl() : null,
                  ),
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(0, 5, 0, 7),
                  width: double.infinity,
                  child: FutureBuilder<List<CityModel>>(
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data != null) {
                        if (isGetCity) {
                          print("DATA:::${snapshot.data!.length}");
                          model = widget.model != null
                              ? snapshot.data![getDropDownSelectedPosition2(
                                  widget.model!.City_ID, snapshot.data!)]
                              : snapshot.data![0];
                        }
                        return DropdownButtonHideUnderline(
                          child: DropdownButton2(
                            isExpanded: true,
                            items: snapshot.data!
                                .map((item) => DropdownMenuItem<CityModel>(
                                      value: item,
                                      child: Text(
                                        item.City_NAME.toString(),
                                        style: const TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black,
                                        ),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ))
                                .toList(),
                            value: model,
                            onChanged: (value) {
                              setState(() {
                                isGetCity = false;
                                model = value!;
                              });
                            },
                            icon: const Icon(
                              Icons.arrow_drop_down_outlined,
                            ),
                            buttonDecoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14),
                              border: Border.all(
                                color: Colors.black26,
                              ),
                              color: Colors.blue.shade100,
                            ),
                            dropdownDecoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(14),
                              color: Colors.blue.shade100,
                            ),
                          ),
                        );
                      } else {
                        return Container();
                      }
                    },
                    future:
                        isGetCity ? MyDatabase().getCityListFromTbl() : null,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: nameController,
                    validator: (value) {
                      if (value == null || value.trim().length == 0) {
                        return 'Enter Valid Name';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Enter Name',
                    ),
                    textInputAction: TextInputAction.next,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: genderController,
                    validator: (value) {
                      if (value == null || value.trim().length == 0) {
                        return 'Enter Valid Gender';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Enter Gender',
                    ),
                    textInputAction: TextInputAction.next,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: heightController,
                    validator: (value) {
                      if (value == null || value.trim().length == 0) {
                        return 'Enter Valid Height';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Enter Height',
                    ),
                    textInputAction: TextInputAction.next,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: weightController,
                    validator: (value) {
                      if (value == null || value.trim().length == 0) {
                        return 'Enter Valid Weight';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Enter Weight',
                    ),
                    textInputAction: TextInputAction.next,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: dateController,
                    validator: (value) {
                      if (value == null || value.trim().length == 0) {
                        return 'Enter Valid DOB';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Enter DOB',
                    ),
                    textInputAction: TextInputAction.next,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: educationController,
                    validator: (value) {
                      if (value == null || value.trim().length == 0) {
                        return 'Enter Valid Education';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Enter Education',
                    ),
                    textInputAction: TextInputAction.next,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: professionController,
                    validator: (value) {
                      if (value == null || value.trim().length == 0) {
                        return 'Enter Valid Profession';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Enter Profession',
                    ),
                    textInputAction: TextInputAction.next,
                  ),
                ),
                Container(
                  padding: const EdgeInsets.all(10),
                  child: TextFormField(
                    controller: religionController,
                    validator: (value) {
                      if (value == null || value.trim().length == 0) {
                        return 'Enter Valid Religion';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Enter Religion',
                    ),
                    textInputAction: TextInputAction.done,
                  ),
                ),
                SizedBox(
                  width: 100,
                  child: Container(
                    margin: EdgeInsets.fromLTRB(0, 5, 0, 10),
                    //padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(color: Colors.blue),
                    child: TextButton(
                      onPressed: () async {
                        if (_formKey.currentState!.validate()) {
                          print('\n::${model.City_ID.toString()}' +
                              '\n::${model1.Country_ID.toString()}' +
                              '\n::${model2.State_ID.toString()}'
                                  '\n::${nameController.text.toString()}'
                                  '\n::${genderController.text.toString()}'
                                  '\n::${heightController.text.toString()}'
                                  '\n::${weightController.text.toString()}'
                                  '\n::${dateController.text.toString()}'
                                  '\n::${educationController.text.toString()}'
                                  '\n::${professionController.text.toString()}'
                                  '\n::${religionController.text.toString()}');
                          if (model.City_ID == -1) {
                            showAlertDialog(context);
                          } else {
                           // insertUser();
                            //aduser();
                            await MyDatabase().upsertIntoUserTable(
                                countryId: model1.Country_ID.toString(),
                                stateId: model2.State_ID.toString(),
                                cityId: model.City_ID.toString(),
                                userName: nameController.text.toString(),
                                userGender: genderController.text.toString(),
                                userHeight: heightController.text.toString(),
                                userWeight: weightController.text.toString(),
                                dob: dateController.text.toString(),
                                userEducation:
                                    educationController.text.toString(),
                                userProfession:
                                    professionController.text.toString(),
                                userReligion:
                                    religionController.text.toString(),
                                User_ID: widget.model != null
                                    ? widget.model!.User_ID
                                    : -1);
                          }
                        }
                      },
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.white, fontSize: 18),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> aduser() async {
    final dio = Dio();
    final client = RestClient(dio);

    client
        .addUser(
      nameController.text,
      dateController.text,
      genderController.text,
      heightController.text,
      weightController.text,
      model1.Country_NAME,
      model2.State_NAME,
      model.City_NAME,
      religionController.text,
      educationController.text,
      professionController.text,
    )
        .then(
      (value) {
        print("RESPONCE:::${value.toString()}");
      },
    );
  }

  showAlertDialog(BuildContext context) {
    Widget okButton = TextButton(
      child: Text("OK"),
      onPressed: () {
        dispose();
      },
    );

    AlertDialog alert = AlertDialog(
      title: Text('Alert'),
      content: Text("please select city"),
      actions: [
        okButton,
      ],
    );

    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<void> insertUser() async {
    Map map = {};

    map["Name"] = nameController.text;
    map["DOB"] = dateController.text;
    map["Gender"] = genderController.text;
    map["Height"] = heightController.text;
    map["Weigth"] = weightController.text;
    map["Country"] = model1.toString();
    map["State"] = model2.toString();
    map["City"] = model.toString();
    map["Religion"] = religionController.text;
    map["Education"] = educationController.text;
    map["Profeasion"] = professionController.text;

    var Response1 = await http.post(
      Uri.parse("https://62da4a135d893b27b2f4cdf5.mockapi.io/Faculty/"),
    );
    print(Response1.body);
  }
}
