import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrimony_app/model/user_model.dart';

class SearchPage extends StatefulWidget {
  @override
  State<SearchPage> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('Search User'),
        ),
        body: Column(
          children: [
            SizedBox(
              // width: 310,
              //height: 70,
              child: Container(
                margin: EdgeInsets.fromLTRB(12, 20, 10, 0),
                child: TextField(
                  textAlign: TextAlign.left,
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.search),
                      border: const OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                          Radius.circular(23.0),
                        ),
                      ),
                      filled: true,
                      hintStyle: new TextStyle(color: Colors.grey[600]),
                      hintText: "Search User",
                      contentPadding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      fillColor: Colors.white),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
