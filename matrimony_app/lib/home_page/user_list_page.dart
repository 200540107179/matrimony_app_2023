import 'package:flutter/material.dart';
import 'package:matrimony_app/database/database.dart';
import 'package:matrimony_app/home_page/add_user_page.dart';
import 'package:matrimony_app/model/city_model.dart';
import 'package:matrimony_app/model/state_model.dart';
import 'package:matrimony_app/model/user_model.dart';
import 'package:sqflite/sqflite.dart';

class UserListPage extends StatefulWidget {
  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  TextEditingController controller = TextEditingController();

  List<UserModel> localList = [];
  List<UserModel> searchList = [];
  bool isGetData = true;

  // late Future<List<Map<String, Object?>>> userListFuture;

  @override
  void initState() {
    super.initState();
    controller.addListener(
      () {
        print(':::${controller.text}');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text('User List'),
        ),
        body: Container(
          color: Colors.white,
          child: FutureBuilder<List<UserModel>>(
            builder: (context, snapshot) {
              if (snapshot != null && snapshot.hasData) {
                if (isGetData) {
                  localList.addAll(snapshot.data!);
                  searchList.addAll(localList);
                }
                print('Local:List::${localList.length}');
                isGetData = false;
                return Column(
                  children: [
                    SizedBox(
                      // width: 310,
                      //height: 70,
                      child: Container(
                        margin: EdgeInsets.fromLTRB(12, 20, 10, 0),
                        child: TextField(
                          controller: controller,
                          onChanged: (value) {
                            searchList.clear();
                            print('::value::${value}');

                            for (int i = 0; i < localList.length; i++) {
                              if (localList[i]
                                  .User_NAME
                                  .toLowerCase()
                                  .contains(value.toLowerCase())) {
                                searchList.add(localList[i]);
                              }
                            }
                            setState(() {});
                          },
                          textAlign: TextAlign.left,
                          decoration: InputDecoration(
                              prefixIcon: Icon(Icons.search),
                              border: const OutlineInputBorder(
                                borderRadius: BorderRadius.all(
                                  Radius.circular(23.0),
                                ),
                              ),
                              filled: true,
                              hintStyle: new TextStyle(color: Colors.grey[600]),
                              hintText: "Search User",
                              contentPadding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                              fillColor: Colors.white),
                        ),
                      ),
                    ),
                    Expanded(
                      child: ListView.builder(
                        padding: EdgeInsets.all(5),
                        itemBuilder: (context, index) {
                          return Card(
                            color: Colors.blue.shade50,
                            elevation: 3,
                            borderOnForeground: true,
                            child: Container(
                              padding: EdgeInsets.all(5),
                              child: Row(
                                children: [
                                  Expanded(
                                    child: Row(
                                      children: [
                                        Expanded(
                                          child: Text(
                                            (searchList![index]
                                                .User_NAME
                                                .toString()),
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                        IconButton(
                                          icon: Icon(
                                            Icons.edit,
                                            color: Colors.black38,
                                          ),
                                          onPressed: () {
                                            Navigator.of(context).push(
                                              MaterialPageRoute(
                                                builder: (context) => AddUser(
                                                    model: searchList![index]),
                                              ),
                                            );
                                          },
                                        ),
                                        IconButton(
                                          icon: Icon(
                                            Icons.delete_rounded,
                                            color: Colors.black38,
                                          ),
                                          onPressed: () {
                                            showAlertDialog(context, index);
                                          },
                                        ),
                                        IconButton(
                                          icon: Icon(
                                            searchList[index].isFavouriteUser
                                                ? Icons.favorite
                                                : Icons.favorite_border,
                                            color: Colors.red,
                                          ),
                                          onPressed: () {
                                            setState(
                                              () {
                                                searchList[index]
                                                        .isFavouriteUser =
                                                    !searchList[index]
                                                        .isFavouriteUser;
                                              },
                                            );
                                          },
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                        itemCount: searchList!.length,
                      ),
                    ),
                  ],
                );
              } else {
                return Center(
                  child: Text('No User Found'),
                );
              }
            },
            future: isGetData ? MyDatabase().getUserListFromTbl() : null,
          ),
        ),
      ),
    );
  }

  showAlertDialog(BuildContext context, index) {
    Widget yesButton = TextButton(
      child: Text("Yes"),
      onPressed: () async {
        int deletedUserID = await MyDatabase()
            .deleteUserFromUserTable(localList[index].User_ID);
        if (deletedUserID > 0) {
          localList.removeAt(index);
        }
        Navigator.pop(context);
        setState(() {});
      },
    );
    Widget noButton = TextButton(
      child: Text("No"),
      onPressed: () {
        Navigator.pop(context);
      },
    );
    AlertDialog alert = AlertDialog(
      title: Text("Alert"),
      content: Text("Are you sure want to delete?"),
      actions: [
        yesButton,
        noButton,
      ],
    );
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
