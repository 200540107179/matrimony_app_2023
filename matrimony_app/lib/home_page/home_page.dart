import 'dart:ffi';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:matrimony_app/home_page/add_user_page.dart';
import 'package:matrimony_app/home_page/favourite_page.dart';
import 'package:matrimony_app/home_page/search_page.dart';
import 'package:matrimony_app/home_page/user_list_page.dart';
import 'package:matrimony_app/model/user_model.dart';

class Home_page extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage(
                'assets/image/start_page.jpeg',
              ),
              fit: BoxFit.cover,
            ),
          ),
          child: Container(
            alignment: Alignment.center,
            child: Column(
              children: [
                // Expanded(
                //   child: Column(
                //     children: [
                //       Container(
                //         margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                //         // alignment: Alignment.center,
                //         decoration: BoxDecoration(
                //           border: Border.all(width: 2, color: Colors.white),
                //           borderRadius: BorderRadius.circular(25),
                //         ),
                //         child: Padding(
                //           padding: const EdgeInsets.fromLTRB(0, 4, 8, 12),
                //           child: IconButton(
                //             onPressed: () {
                //               Navigator.push(
                //                 context,
                //                 MaterialPageRoute(
                //                   builder: (context) => SearchPage(),
                //                 ),
                //               );
                //             },
                //             icon: Icon(
                //               Icons.search,
                //               color: Colors.white,
                //               size: 45,
                //             ),
                //           ),
                //         ),
                //       ),
                //       Text(
                //         'Search',
                //         style: TextStyle(color: Colors.white, fontSize: 25),
                //       ),
                //     ],
                //   ),
                // ),
                Expanded(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                        // alignment: Alignment.center,
                        decoration: BoxDecoration(
                          border: Border.all(width: 2, color: Colors.white),
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 14, 12),
                          child: IconButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => AddUser(model: null),
                                ),
                              );
                            },
                            icon: Icon(
                              Icons.person_add,
                              color: Colors.white,
                              size: 45,
                            ),
                          ),
                        ),
                      ),
                      Text(
                        'Add User',
                        style: TextStyle(color: Colors.white, fontSize: 25),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                        // alignment: Alignment.center,
                        decoration: BoxDecoration(
                          border: Border.all(width: 2, color: Colors.white),
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 4, 10, 12),
                          child: IconButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => UserListPage(),
                                ),
                              );
                            },
                            icon: Icon(
                              Icons.list,
                              color: Colors.white,
                              size: 45,
                            ),
                          ),
                        ),
                      ),
                      Text(
                        'User List',
                        style: TextStyle(color: Colors.white, fontSize: 25),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.fromLTRB(0, 50, 0, 0),
                        // alignment: Alignment.center,
                        decoration: BoxDecoration(
                          border: Border.all(width: 2, color: Colors.white),
                          borderRadius: BorderRadius.circular(25),
                        ),
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 4, 12, 12),
                          child: IconButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => FavouritePage(),
                                ),
                              );
                            },
                            icon: Icon(
                              Icons.favorite_rounded,
                              color: Colors.white,
                              size: 45,
                            ),
                          ),
                        ),
                      ),
                      Text(
                        'Favourite',
                        style: TextStyle(color: Colors.white, fontSize: 25),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
