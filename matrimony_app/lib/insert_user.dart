import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Insert_User extends StatefulWidget {
  @override
  State<Insert_User> createState() => _Insert_UserState();
}

var formKey = GlobalKey<FormState>();
var nameController = TextEditingController();
var genderController = TextEditingController();
var cityController = TextEditingController();

class _Insert_UserState extends State<Insert_User> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Form(
          key: formKey,
          child: Column(
            children: [
              TextFormField(
                decoration: InputDecoration(hintText: "Enter Name"),
                validator: (value) {
                  if (value != null && value.isEmpty) {
                    return "Enter Valid Name";
                  }
                },
                controller: nameController,
              ),
              TextFormField(
                decoration: InputDecoration(hintText: "Enter Gender"),
                validator: (value) {
                  if (value != null && value.isEmpty) {
                    return "Enter Valid Gender";
                  }
                },
                controller: genderController,
              ),
              TextFormField(
                decoration: InputDecoration(hintText: "Enter City"),
                validator: (value) {
                  if (value != null && value.isEmpty) {
                    return "Enter Valid City";
                  }
                },
                controller: cityController,
              ),
              TextButton(
                onPressed: () {
                  if (formKey.currentState!.validate()) {
                    insertuser()
                        .then((value) => Navigator.of(context).pop(true));
                  }
                },
                child: Text("Submit"),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> insertuser() async {
    Map map = {};

    map["Name"] = nameController.text;
    map["City"] = cityController.text;
    map["Gender"] = genderController.text;

    var responce1 = await http
        .post(Uri.parse("https://62da4a135d893b27b2f4cdf5.mockapi.io/Faculty"));
    print(responce1.body);
  }
}
