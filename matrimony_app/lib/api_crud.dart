import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:matrimony_app/insert_user.dart';
import 'package:matrimony_app/insertuser.dart';

class Api_CRUD extends StatefulWidget {
  @override
  State<Api_CRUD> createState() => _Api_CRUDState();
}

class _Api_CRUDState extends State<Api_CRUD> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: const Text("Api Demo"),
          actions: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: InkWell(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) {
                      return Insert_user(null);
                    },
                  )).then((value) {
                    if (value == true) {
                      setState(() {});
                    }
                  });
                },
                child: Icon(Icons.add),
              ),
            ),
          ],
        ),
        body: FutureBuilder<http.Response>(
          builder: (context, snapshot) {
            if (snapshot.data != null && snapshot.hasData) {
              // List<dynamic> datas = jsonDecode(snapshot.data!.body.toString());
              // datas.reversed;
              return ListView.builder(
                itemBuilder: (context, index) {
                  return Container(
                    //padding: EdgeInsets.all(5),
                    child: InkWell(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) {
                            return Insert_user(jsonDecode(
                                snapshot.data!.body.toString())[index]);
                          },
                        )).then((value) {
                          if (value == true) {
                            setState(() {});
                          }
                        });
                      },
                      child: Card(
                        color: index % 2 == 0 ? Colors.white12 : Colors.white38,
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                (jsonDecode(snapshot.data!.body.toString())[
                                        index]['Name'])
                                    .toString(),
                                style: const TextStyle(
                                    fontSize: 18, fontWeight: FontWeight.bold),
                              ),
                            ),
                            IconButton(
                              onPressed: () {
                                showDeleteAlert((jsonDecode(
                                        snapshot.data!.body.toString())[index]
                                    ['id']));
                              },
                              icon: Icon(
                                Icons.delete,
                                color: Colors.red.shade400,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
                itemCount: jsonDecode(snapshot.data!.body).length,
              );
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
          future: getData(),
        ),
      ),
    );
  }

  Future<http.Response> getData() async {
    var Response = await http
        .get(Uri.parse("https://62da4a135d893b27b2f4cdf5.mockapi.io/Faculty"));
    return Response;
  }

  Future<http.Response> deleteitem(id) async {
    var response = await http.delete(
      Uri.parse("https://62da4a135d893b27b2f4cdf5.mockapi.io/Faculty/$id"),
    );
    return response;
  }

  void showDeleteAlert(id) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text("Alert!"),
          content: Text("Are you sure want to delete this record?"),
          actions: [
            TextButton(
              onPressed: () async {
                http.Response res = await deleteitem(id);

                if (res.statusCode == 200) {
                  setState(() {});
                }
                Navigator.of(context).pop();
              },
              child: Text("Yes"),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text("No"),
            ),
          ],
        );
      },
    );
  }
}
