import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:dio/dio.dart';
import 'package:matrimony_app/api/rest_client.dart';
import 'package:matrimony_app/home_page/add_user_page.dart';
import 'package:matrimony_app/insert_user.dart';
import 'package:matrimony_app/model/user_list_model/user_model.dart';

class ApiDemo extends StatefulWidget {
  @override
  State<ApiDemo> createState() => _ApiDemoState();
}

class _ApiDemoState extends State<ApiDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:const Text("Api Demo"),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: InkWell(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    builder: (context) => Insert_User(),
                  ),
                );
              },
              child: Icon(Icons.add),
            ),
          ),
        ],
      ),
      body: FutureBuilder<http.Response>(
        builder: (context, snapshot) {
          if (snapshot.data != null && snapshot.hasData) {
            return ListView.builder(
              itemBuilder: (context, index) {
                return Container(
                  //padding: EdgeInsets.all(5),
                  child: Card(
                    color: index % 2 == 0 ? Colors.white12 : Colors.white38,
                    child: Row(
                      children: [
                        Expanded(
                          child: Text(
                            (jsonDecode(snapshot.data!.body.toString())[index]
                                    ['Name'])
                                .toString(),
                            style: const TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ),
                        IconButton(
                          onPressed: () {},
                          icon: Icon(
                            Icons.delete,
                            color: Colors.red.shade400,
                          ),
                        ),
                      ],
                    ),
                  ),
                );
              },
              itemCount: jsonDecode(snapshot.data!.body).length,
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
        future: callApi(),
      ),
    );
  }

  Future<http.Response> callApi() async {
    var Response = await http.get(
      Uri.parse("https://62da4a135d893b27b2f4cdf5.mockapi.io/Faculty/"),
    );
    return Response;
  }

// Future<UserListModel> getUsers() async {
//   final dio = Dio();
//   final client = RestClient(dio);
//   UserListModel data =
//       UserListModel.fromJson(jsonDecode(await client.getUsers()));
//   print("DATA:::${data.resultList!.length}");
//   return data;
// }

// void deleteUser() {
//   final dio = Dio();
//   final client = RestClient(dio);
// }
}
