import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

part 'rest_client.g.dart';

@RestApi(baseUrl: "https://62da4a135d893b27b2f4cdf5.mockapi.io/")
abstract class RestClient {
  factory RestClient(Dio dio, {String baseUrl}) = _RestClient;

  @GET("/Faculty")
  Future<String> getUsers();

  @POST("/Faculty")
  Future<String> addUser(
    @Field('Name') Name,
    @Field('DOB') DOB,
    @Field('Gender') Gender,
    @Field('Height') Height,
    @Field('Weigth') Weigth,
    @Field('Country') Country,
    @Field('State') State,
    @Field('City') City,
    @Field('Religion') Religion,
    @Field('Education') Education,
    @Field('Profeasion') Profeasion,
  );

  @DELETE("/Faculty/{id}")
  Future<void> deleteTask(@Path("id") String id);
}
