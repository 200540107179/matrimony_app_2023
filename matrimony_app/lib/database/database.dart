import 'dart:io';
import 'package:flutter/services.dart';
import 'package:matrimony_app/model/city_model.dart';
import 'package:matrimony_app/model/country_model.dart';
import 'package:matrimony_app/model/education_model.dart';
import 'package:matrimony_app/model/profession_model.dart';
import 'package:matrimony_app/model/religion_model.dart';
import 'package:matrimony_app/model/state_model.dart';
import 'package:matrimony_app/model/user_model.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class MyDatabase {
  Future<Database> initDatabase() async {
    Directory appDocDir = await getApplicationDocumentsDirectory();
    String databasePath = join(appDocDir.path, 'Matrimony_app.db');
    return await openDatabase(
      databasePath,
      version: 2,
    );
  }

  Future<void> copyPasteAssetFileToRoot() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "Matrimony_app.db");

    if (FileSystemEntity.typeSync(path) == FileSystemEntityType.notFound) {
      ByteData data =
          await rootBundle.load(join('assets/database', 'Matrimony_app.db'));
      List<int> bytes =
          data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await new File(path).writeAsBytes(bytes);
    }
  }

  Future<void> upsertIntoUserTable(
      {countryId,
      stateId,
      cityId,
      userName,
      userGender,
      userHeight,
      userWeight,
      dob,
      userEducation,
      userProfession,
      userReligion,
      User_ID}) async {
    Database db = await initDatabase();
    Map<String, Object?> map = Map();

    map['User_NAME'] = userName;
    map['User_DOB'] = dob;
    map['User_GENDER'] = userGender;
    map['User_HEIGHT'] = userHeight;
    map['User_WEIGHT'] = userWeight;
    map['Country_ID'] = countryId;
    map['State_ID'] = stateId;
    map['City_ID'] = cityId;
    map['Religion_ID'] = userReligion;
    map['Education_ID'] = userEducation;
    map['Profession_ID'] = userProfession;

    if (User_ID != -1) {
      //map['User_ID'] = User_ID;
      await db.update(
        'TBL_USER',
        map,
        where: 'User_ID = ?',
        whereArgs: [User_ID],
      );
    } else {
      await db.insert('TBL_USER', map);
    }
  }

  Future<List<UserModel>> getUserListFromTbl() async {
    List<UserModel> userList = [];
    Database db = await initDatabase();
    List<Map<String, Object?>> data =
        await db.rawQuery('SELECT * FROM TBL_USER');

    print("USER LIST ::: ${data.length}");

    for (int i = 0; i < data.length; i++) {
      UserModel model = UserModel();

      model.User_ID = data[i]['User_ID'] as int;
      model.User_NAME = data[i]['User_NAME'].toString();
      model.User_DOB = data[i]['User_DOB'].toString();
      model.User_GENDER = data[i]['User_GENDER'].toString();
      model.User_HEIGHT = data[i]['User_HEIGHT'].toString();
      model.User_WEIGHT = data[i]['User_WEIGHT'].toString();
      model.Country_ID = data[i]['Country_ID'] as int;
      model.State_ID = data[i]['State_ID'] as int;
      model.City_ID = data[i]['City_ID'] as int;
      model.Religion_ID = data[i]['Religion_ID'].toString();
      model.Education_ID = data[i]['Education_ID'].toString();
      model.Profession_ID = data[i]['Profession_ID'].toString();
      model.isFavouriteUser = false;
      userList.add(model);
    }
    return userList;
  }

  Future<List<CityModel>> getCityListFromTbl() async {
    List<CityModel> cityList = [];
    Database db = await initDatabase();
    List<Map<String, Object?>> data =
        await db.rawQuery('SELECT * FROM TBL_CITY');
    CityModel model = CityModel();
    model.City_ID = -1;
    model.City_NAME = 'Select City';
    cityList.add(model);
    for (int i = 0; i < data.length; i++) {
      model = CityModel();
      model.City_ID = data[i]['City_ID'] as int;
      model.City_NAME = data[i]['City_NAME'].toString();
      cityList.add(model);
    }

    return cityList;
  }

  Future<List<CountryModel>> getCountryListFromTbl() async {
    List<CountryModel> countryList = [];
    Database db = await initDatabase();
    List<Map<String, Object?>> data =
        await db.rawQuery('SELECT * FROM TBL_COUNTRY');
    CountryModel model = CountryModel();
    model.Country_ID = -1;
    model.Country_NAME = 'Select Country';
    countryList.add(model);
    for (int i = 0; i < data.length; i++) {
      CountryModel model = CountryModel();
      model.Country_ID = data[i]['Country_ID'] as int;
      model.Country_NAME = data[i]['Country_NAME'].toString();
      countryList.add(model);
    }

    return countryList;
  }

  Future<List<StateModel>> getStateListFromTbl() async {
    List<StateModel> stateList = [];
    Database db = await initDatabase();
    List<Map<String, Object?>> data =
        await db.rawQuery('SELECT * FROM TBL_STATE');
    StateModel model = StateModel();
    model.State_ID = -1;
    model.State_NAME = 'Select State';
    stateList.add(model);
    for (int i = 0; i < data.length; i++) {
      StateModel model = StateModel();
      model.State_ID = data[i]['State_ID'] as int;
      model.State_NAME = data[i]['State_NAME'].toString();
      stateList.add(model);
    }
    return stateList;
  }

  Future<int> deleteUserFromUserTable(User_ID) async {
    Database db = await initDatabase();
    int deletedid = await db.delete(
      'TBL_USER',
      where: 'User_ID = ?',
      whereArgs: [User_ID],
    );
    return deletedid;
  }

// Future<List<EducationModel>> getEducationListFromTbl() async {
//   List<EducationModel> educationList = [];
//   Database db = await initDatabase();
//   List<Map<String, Object?>> data =
//       await db.rawQuery('SELECT * FROM TBL_EDUCATION');
//   for (int i = 0; i < data.length; i++) {
//     EducationModel model = EducationModel();
//     model.Education_ID = data[i]['Education_ID'] as int;
//     model.Education_NAME = data[i]['Education_NAME'].toString();
//     educationList.add(model);
//   }
//
//   return educationList;
// }
//
// Future<List<ProfessionModel>> getprofessionListFromTbl() async {
//   List<ProfessionModel> professionList = [];
//   Database db = await initDatabase();
//   List<Map<String, Object?>> data =
//       await db.rawQuery('SELECT * FROM TBL_PROFESSION');
//   for (int i = 0; i < data.length; i++) {
//     ProfessionModel model = ProfessionModel();
//     model.Profession_ID = data[i]['Profession_ID'] as int;
//     model.Profession_NAME = data[i]['Profession_NAME'].toString();
//     professionList.add(model);
//   }
//
//   return professionList;
// }
//
// Future<List<ReligionModel>> getreligionListFromTbl() async {
//   List<ReligionModel> religionList = [];
//   Database db = await initDatabase();
//   List<Map<String, Object?>> data =
//       await db.rawQuery('SELECT * FROM TBL_RELIGION');
//   for (int i = 0; i < data.length; i++) {
//     ReligionModel model = ReligionModel();
//     model.Religion_ID = data[i]['Religion_ID'] as int;
//     model.Religion_Name = data[i]['Religion_Name'].toString();
//     religionList.add(model);
//   }
//
//   return religionList;
// }
}
